package input

import (
	"errors"
	"fmt"
	"kalimasi/doc"
	"kalimasi/util"

	"github.com/globalsign/mgo/bson"
)

type UpdateCompanyAudit struct {
	IsAudit      bool `json:"isAudit"`
	AuditProcess []struct {
		Typ   string `json:"type"`
		Value string `json:"value"`
	} `json:"auditProcess"`
}

func (cu *UpdateCompanyAudit) ToDoc() doc.DocInter {
	ap := []doc.AuditProcess{}
	for _, p := range cu.AuditProcess {
		ap = append(ap, doc.AuditProcess{
			Typ:   p.Typ,
			Value: p.Value,
		})
	}
	as := doc.AuditSetting{
		IsAudit:      cu.IsAudit,
		AuditProcess: ap,
	}
	return &doc.CompanyAudit{
		AuditSetting: as,
	}
}

func (cu *UpdateCompanyAudit) Validate() error {
	if cu.IsAudit && len(cu.AuditProcess) == 0 {
		return errors.New("auditProcess must setting when audit")
	}
	for _, ap := range cu.AuditProcess {
		if !util.IsStrInList(ap.Typ, doc.AuditProcTypAcc, doc.AuditProcTypPerm) {
			return errors.New("invalid audit process type")
		}
		if ap.Value == "" {
			return errors.New("invalid audit process value")
		}
		if ap.Typ == doc.AuditProcTypAcc {
			// value 為ObjectID
			if !bson.IsObjectIdHex(ap.Value) {
				return errors.New("value must be userID when type is account")
			}
		}
		if ap.Typ == doc.AuditProcTypPerm &&
			!util.IsStrInList(ap.Value, doc.UserPermOwn, doc.UserPermPro, doc.UserPermUser) {
			msg := util.StrAppend("[", doc.UserPermOwn, ",", doc.UserPermPro, ",", doc.UserPermUser, "]")
			return fmt.Errorf("value must be one of %s when type is permission", msg)
		}
	}
	return nil
}

type UpdateAudit struct {
	State   string
	Message string
}

func (cu *UpdateAudit) Validate() error {
	if !util.IsStrInList(cu.State, doc.AuditStateReject, doc.AuditStatePass) {
		return errors.New("invalid audit state")
	}
	return nil
}

type QueryAudit struct {
	Typ       string
	State     string
	CompanyID bson.ObjectId
}

func (qb *QueryAudit) Validate() error {
	if !qb.CompanyID.Valid() {
		return errors.New("invalid companyID")
	}
	if qb.State != "" && !util.IsStrInList(qb.State, doc.AuditStateNew, doc.AuditStateProcess,
		doc.AuditStateReject, doc.AuditStatePass) {
		return errors.New("invalid audit state")
	}
	return nil
}

func (qb *QueryAudit) GetMgoQuery() bson.M {
	q := bson.M{}
	if qb.State != "" {
		q["state"] = qb.State
	}
	if qb.Typ != "" {
		q["typ"] = qb.Typ
	}
	return q
}
