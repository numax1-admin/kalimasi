package input

import (
	"errors"
	"kalimasi/doc"
	"kalimasi/util"
)

type CreateBmsManager struct {
	Account  string `json:"account"`
	Password string `json:"password"`
	Name     string `json:"name"`
	Phone    string `json:"phone"`
}

func (cbm *CreateBmsManager) Validate() error {
	if _, err := util.IsValidPwd(cbm.Password); err != nil {
		return errors.New("illegalPwd: " + err.Error())
	}
	if _, err := util.IsLegalPhoneNumber(cbm.Phone); err != nil {
		return errors.New("illegalPhoneNumber: " + err.Error())
	}
	return nil
}

func (cbm *CreateBmsManager) ToDoc() doc.DocInter {
	return &doc.User{
		Email:       cbm.Account,
		DisplayName: cbm.Name,
		Pwd:         util.MD5(cbm.Password),
		PhoneNumber: cbm.Phone,
		Service:     []string{doc.UserServiceBSM},
		Disabled:    true,
	}
}
