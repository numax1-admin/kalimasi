/*
 * 收支預算表
 */
package report

import (
	"encoding/json"
	"fmt"
	"io"
	"strings"
	"time"

	"kalimasi/report/pdf"

	"github.com/leekchan/accounting"
)

type BalanceBudgetItem struct {
	Category    string
	SubCategory string
	Item        string
	AccTerm     string
	Amount      int
	LastAmount  *int
	Increase    int
	Decrease    int
	Desc        string
}

type reportStyle interface {
	pdf.TableStyle
	GetTitle() pdf.TextStyle    // 報表抬頭
	GetSubTitle() pdf.TextStyle // 副標題
	GetContent() pdf.TextStyle
	TextBlock(fontSize int) pdf.TextBlockStyle
	UTextBlock(fontSize int) pdf.TextBlockStyle
}

type BalanceBudget struct {
	Title     string
	StartDate time.Time
	EndDate   time.Time
	Item      []*BalanceBudgetItem

	Style reportStyle       `json:"-"`
	Font  map[string]string `json:"-"`

	commonReport
}

func (bb *BalanceBudget) AddItem(bbi *BalanceBudgetItem) {
	bb.Item = append(bb.Item, bbi)
}

func (bb *BalanceBudget) AddItems(bbi []*BalanceBudgetItem) {
	bb.Item = append(bb.Item, bbi...)
}

func (bb *BalanceBudget) getDateRange() string {
	const format = "中華民國%d年%d月%d日至%d年%d月%d日"
	return fmt.Sprintf(format,
		bb.StartDate.Year()-1911, bb.StartDate.Month(), bb.StartDate.Day(),
		bb.EndDate.Year()-1911, bb.EndDate.Month(), bb.EndDate.Day(),
	)
}

func currencyNoMinusFormat(amount *int) string {
	if amount == nil {
		return ""
	}
	r := *amount
	if r < 0 {
		r = r * -1
	}
	return currencyFormat(&r)
}

func currencyFormat(amount *int) string {
	if amount == nil {
		return ""
	}
	ac := accounting.Accounting{Symbol: "", Precision: 0}
	return ac.FormatMoney(*amount)
}

func (bb *BalanceBudget) Json(w io.Writer) error {
	return json.NewEncoder(w).Encode(bb)
}

func (bb *BalanceBudget) PDF(w io.Writer) error {
	const subtitle = "收支預算表"
	p := pdf.GetA4PDF(bb.Font, 20, 20, 20, 20)
	p.AddPage()
	style := bb.Style
	// page one report
	p.Text(bb.Title, style.GetTitle(), pdf.AlignCenter)
	p.Br(25)
	p.Text(subtitle, style.GetSubTitle(), pdf.AlignCenter)
	p.Br(25)
	p.Text(bb.getDateRange(), style.GetSubTitle(), pdf.AlignRight)
	p.Br(25)
	p.Line(2)
	p.Br(5)

	nt := pdf.GetNormalTable()
	ha := bb.getHeaderAry()
	for _, h := range ha {
		nt.AddHeader(h)
	}

	for _, i := range bb.Item {
		increaseStr, decreaseStr := "", ""
		if i.Increase > 0 {
			increaseStr = currencyFormat(&i.Increase)
		}
		if i.Decrease > 0 {
			decreaseStr = currencyFormat(&i.Decrease)
		}
		autoDesc, line := autoEnter(i.Desc, 10)
		fl := float64(line)
		rowHeight := (fl-1)*0.5*20 + 20
		nt.AddRow([]*pdf.TableColumn{
			pdf.GetTableColumn(20, rowHeight, singleAlignCenter, i.Category),
			pdf.GetTableColumn(20, rowHeight, singleAlignCenter, i.SubCategory),
			pdf.GetTableColumn(20, rowHeight, singleAlignCenter, i.Item),
			pdf.GetTableColumn(90, rowHeight, singleAlignCenter, i.AccTerm),
			pdf.GetTableColumn(80, rowHeight, singleAlignRight, currencyFormat(&i.Amount)),
			pdf.GetTableColumn(80, rowHeight, singleAlignRight, currencyFormat(i.LastAmount)),
			pdf.GetTableColumn(75, rowHeight, singleAlignRight, increaseStr),
			pdf.GetTableColumn(75, rowHeight, singleAlignRight, decreaseStr),
			pdf.GetTableColumn(100, rowHeight, singleAlignCenter, autoDesc),
		})
	}
	nt.Draw(&p, style)

	p.Br(5)
	p.Text("(以下請核章)", style.GetSubTitle(), pdf.AlignLeft)
	p.Br(20)
	p.RectFillColor("團體負責人：", style.TextBlock(12), 400, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillColor("秘書長：", style.TextBlock(12), 100, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.Br(30)
	p.RectFillColor("會計：", style.TextBlock(12), 400, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillColor("製表：", style.TextBlock(12), 100, 20, pdf.AlignLeft, pdf.ValignMiddle)
	return p.Write(w)
}

// 自動換行
func autoEnter(m string, l int) (string, int) {
	runeM := []rune(m)

	ml := len(runeM)
	if ml < l {
		return m, 1
	}
	var result []string
	line := 1
	for ml > l {
		result = append(result, string(runeM[0:l]))
		runeM = runeM[l:]
		ml = len(runeM)
		line++
	}
	result = append(result, string(runeM))
	return strings.Join(result, "\n"), line
}

func (bb *BalanceBudget) getHeaderAry() []pdf.TableHeaderColumn {
	var ha []pdf.TableHeaderColumn
	ha = append(ha,
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"科目"},
				Width:  150,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"預算數"},
				Width:  80,
				Height: 40,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"上年度預算數"},
				Width:  80,
				Height: 40,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"本年度與上年度預算比較數"},
				Width:  150,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"說明"},
				Width:  100,
				Height: 40,
			}},
	)
	ha[0].AddSub("款", 20, 20)
	ha[0].AddSub("項", 20, 20)
	ha[0].AddSub("目", 20, 20)
	ha[0].AddSub("科目", 90, 20)

	ha[3].AddSub("增加", 75, 20)
	ha[3].AddSub("減少", 75, 20)

	return ha
}

type BalanceBudgetStyle string

func (ds *BalanceBudgetStyle) GetTitle() pdf.TextStyle {
	return pdf.TextStyle{
		Font:     "tw-m",
		FontSize: 16,
		Color:    pdf.ColorBlack,
	}
}

func (ds *BalanceBudgetStyle) GetSubTitle() pdf.TextStyle {
	return pdf.TextStyle{
		Font:     "tw-r",
		FontSize: 12,
		Color:    pdf.ColorBlack,
	}
}

func (ds *BalanceBudgetStyle) GetContent() pdf.TextStyle {
	return pdf.TextStyle{
		Font:     "tw-r",
		FontSize: 12,
		Color:    pdf.ColorBlack,
	}
}

func (ds *BalanceBudgetStyle) Header() pdf.TextBlockStyle {
	return pdf.TextBlockStyle{
		TextStyle: pdf.TextStyle{
			Font:     "tw-m",
			FontSize: 12,
			Color:    pdf.ColorBlack,
		},
		BackGround: pdf.ColorWhite,
	}
}

func (ds *BalanceBudgetStyle) Data() pdf.TextBlockStyle {
	return pdf.TextBlockStyle{
		TextStyle: pdf.TextStyle{
			Font:     "tw-r",
			FontSize: 10,
			Color:    pdf.ColorBlack,
		},
		W:          30,
		H:          10.0,
		BackGround: pdf.ColorWhite,
		TextAlign:  "left",
	}
}

func (ds *BalanceBudgetStyle) TextBlock(size int) pdf.TextBlockStyle {
	return pdf.TextBlockStyle{
		TextStyle: pdf.TextStyle{
			Font:     "tw-m",
			FontSize: size,
			Color:    pdf.ColorBlack,
		},
		W:          30,
		H:          10.0,
		BackGround: pdf.ColorWhite,
		TextAlign:  "left",
	}
}

func (ds *BalanceBudgetStyle) UTextBlock(size int) pdf.TextBlockStyle {
	return pdf.TextBlockStyle{
		TextStyle: pdf.TextStyle{
			Font:     "tw-m",
			FontSize: size,
			Style:    "U",
			Color:    pdf.ColorBlack,
		},
		W:          30,
		H:          10.0,
		BackGround: pdf.ColorWhite,
		TextAlign:  "left",
	}
}
