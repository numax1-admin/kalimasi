/*
 * 日記帳
 */
package report

import (
	"encoding/json"
	"fmt"
	"io"
	"time"

	"kalimasi/doc"
	"kalimasi/report/pdf"

	"github.com/leekchan/accounting"
)

type JournalAcc struct {
	AccCode      string
	AccName      string
	CustomerCode string
	Customer     string
	Summary      string
	Amount       int
	Typ          string // debit or credit
}

func (ja *JournalAcc) getAccItem() string {
	const format = "%s %s"
	return fmt.Sprintf(format,
		ja.AccCode, ja.AccName,
	)
}

func (ja *JournalAcc) getDebitAmount() int {
	if ja.Typ != doc.TypeAccountDebit {
		return 0
	}
	return ja.Amount
}

func (ja *JournalAcc) getCreditAmount() int {
	if ja.Typ != doc.TypeAccountCredit {
		return 0
	}
	return ja.Amount
}

func (ja *JournalAcc) getDebitAmountStr() string {
	if ja.Typ != doc.TypeAccountDebit {
		return ""
	}
	return amountFormat(ja.Amount)
}

func (ja *JournalAcc) getCreditAmountStr() string {
	if ja.Typ != doc.TypeAccountCredit {
		return ""
	}
	return amountFormat(ja.Amount)
}

type JournalItem struct {
	Date time.Time
	No   string
	list []*JournalAcc
}

func (ji *JournalItem) AddAcc(ja *JournalAcc) {
	ji.list = append(ji.list, ja)
}

func (ji *JournalItem) getDate() string {
	const format = "%03d-%02d-%02d"
	return fmt.Sprintf(format,
		ji.Date.Year()-1911, ji.Date.Month(), ji.Date.Day(),
	)
}

func (ji *JournalItem) getRows(columnWidth []float64) [][]*pdf.TableColumn {
	i := 0
	var result [][]*pdf.TableColumn
	const empty = ""
	credit, debit := 0, 0
	for _, l := range ji.list {
		credit += l.getCreditAmount()
		debit += l.getDebitAmount()
		if i == 0 {
			result = append(result, []*pdf.TableColumn{
				pdf.GetTableColumn(columnWidth[0], 20, []int{pdf.AlignLeft}, ji.getDate()),
				pdf.GetTableColumn(columnWidth[1], 20, []int{pdf.AlignLeft}, ji.No),
				pdf.GetTableColumn(columnWidth[2], 20, []int{pdf.AlignLeft}, l.getAccItem()),
				pdf.GetTableColumn(columnWidth[3], 20, []int{pdf.AlignLeft}, l.CustomerCode),
				pdf.GetTableColumn(columnWidth[4], 20, []int{pdf.AlignLeft}, l.Customer),
				pdf.GetTableColumn(columnWidth[5], 20, []int{pdf.AlignLeft}, l.Summary),
				pdf.GetTableColumn(columnWidth[6], 20, []int{pdf.AlignRight}, l.getDebitAmountStr()),
				pdf.GetTableColumn(columnWidth[7], 20, []int{pdf.AlignRight}, l.getCreditAmountStr()),
			})
		} else {
			result = append(result, []*pdf.TableColumn{
				pdf.GetTableColumn(columnWidth[0], 20, []int{pdf.AlignLeft}, empty),
				pdf.GetTableColumn(columnWidth[1], 20, []int{pdf.AlignLeft}, empty),
				pdf.GetTableColumn(columnWidth[2], 20, []int{pdf.AlignLeft}, l.getAccItem()),
				pdf.GetTableColumn(columnWidth[3], 20, []int{pdf.AlignLeft}, l.CustomerCode),
				pdf.GetTableColumn(columnWidth[4], 20, []int{pdf.AlignLeft}, l.Customer),
				pdf.GetTableColumn(columnWidth[5], 20, []int{pdf.AlignLeft}, l.Summary),
				pdf.GetTableColumn(columnWidth[6], 20, []int{pdf.AlignRight}, l.getDebitAmountStr()),
				pdf.GetTableColumn(columnWidth[7], 20, []int{pdf.AlignRight}, l.getCreditAmountStr()),
			})
		}
		i++
	}
	result = append(result, []*pdf.TableColumn{
		pdf.GetTableColumn(columnWidth[0], 20, []int{pdf.AlignLeft}, "小計"),
		pdf.GetTableColumn(columnWidth[1], 20, []int{pdf.AlignLeft}, empty),
		pdf.GetTableColumn(columnWidth[2], 20, []int{pdf.AlignLeft}, empty),
		pdf.GetTableColumn(columnWidth[3], 20, []int{pdf.AlignLeft}, empty),
		pdf.GetTableColumn(columnWidth[4], 20, []int{pdf.AlignLeft}, empty),
		pdf.GetTableColumn(columnWidth[5], 20, []int{pdf.AlignLeft}, empty),
		pdf.GetTableColumn(columnWidth[6], 20, []int{pdf.AlignRight}, amountFormat(debit)),
		pdf.GetTableColumn(columnWidth[7], 20, []int{pdf.AlignRight}, amountFormat(credit)),
	})
	return result
}

type Journal struct {
	Title      string
	Create     time.Time
	Start, End time.Time

	StartItem *JournalItem // 開帳資料
	items     []*JournalItem

	Style reportStyle       `json:"-"`
	Font  map[string]string `json:"-"`

	commonReport
}

func (j *Journal) AddItem(ji *JournalItem) {
	j.items = append(j.items, ji)
}

func (j *Journal) getCreateDate() string {
	const format = "製表日期：%d年%d月%d日"
	return fmt.Sprintf(format,
		j.Create.Year()-1911, j.Create.Month(), j.Create.Day(),
	)
}

func (j *Journal) getDateRange() string {
	const format = "期間：%d年%d月%d日 至 %d年%d月%d日"
	return fmt.Sprintf(format,
		j.Start.Year()-1911, j.Start.Month(), j.Start.Day(),
		j.End.Year()-1911, j.End.Month(), j.End.Day(),
	)
}

func (j *Journal) getPage(p int) string {
	const format = "頁次： %d"
	return fmt.Sprintf(format,
		p,
	)
}

func (j *Journal) Json(w io.Writer) error {
	return json.NewEncoder(w).Encode(j)
}

func (j *Journal) PDF(w io.Writer) error {
	const subtitle = "日記帳"
	p := pdf.GetA4HPDF(j.Font, 20, 20, 20, 20)
	style := j.Style
	page := 0
	var nt *pdf.NormalTable
	ha := j.getHeaderAry()
	var columnWidthAry []float64
	for _, h := range ha {
		columnWidthAry = append(columnWidthAry, h.Main.Width)
	}
	createPage := func() {
		p.AddPage()
		page++
		nt = pdf.GetNormalTable()
		for _, h := range ha {
			nt.AddHeader(h)
		}
		p.Text(j.Title, style.GetTitle(), pdf.AlignCenter)
		p.Br(25)
		p.Text(subtitle, style.GetSubTitle(), pdf.AlignCenter)
		p.Br(25)
		p.Text(j.getCreateDate(), style.GetSubTitle(), pdf.AlignLeft)
		p.Text(j.getDateRange(), style.GetSubTitle(), pdf.AlignCenter)
		p.Text(j.getPage(page), style.GetSubTitle(), pdf.AlignRight)
		p.Br(25)
	}
	var rows [][]*pdf.TableColumn
	const maxRows = 20
	countRow := 0
	itemRowLen := 0
	for _, item := range j.items {
		if rows == nil {
			createPage()
		}
		rows = item.getRows(columnWidthAry)
		itemRowLen = len(rows)
		if countRow+itemRowLen >= maxRows {
			nt.Draw(&p, style)
			countRow = 0
			createPage()
		}
		for _, r := range rows {
			nt.AddRow(r)
		}
		countRow += itemRowLen
	}
	if countRow > 0 {
		nt.Draw(&p, style)
	}

	// page one report

	// for _, i := range bb.Item {
	// 	h := i.getHeight()

	// 	nt.AddRow([]*pdf.TableColumn{
	// 		pdf.GetTableColumn(100, h, i.getAssetAlign(), i.assetName()),
	// 		pdf.GetTableColumn(100, h, singleAlignRight, i.assetAmount()),
	// 		pdf.GetTableColumn(100, h, i.getLiabAlign(), i.liabName()...),
	// 		pdf.GetTableColumn(100, h, singleAlignRight, i.liabAmount()...),
	// 	})
	// }

	// p.Br(5)
	// p.Text("(以下請核章)", style.GetSubTitle(), pdf.AlignLeft)
	// p.Br(20)
	// p.RectFillColor("團體負責人：", style.TextBlock(12), 400, 20, pdf.AlignLeft, pdf.ValignMiddle)
	// p.RectFillColor("秘書長：", style.TextBlock(12), 100, 20, pdf.AlignLeft, pdf.ValignMiddle)
	// p.Br(30)
	// p.RectFillColor("會計：", style.TextBlock(12), 400, 20, pdf.AlignLeft, pdf.ValignMiddle)
	// p.RectFillColor("製表：", style.TextBlock(12), 100, 20, pdf.AlignLeft, pdf.ValignMiddle)
	return p.Write(w)
}

func (j *Journal) getHeaderAry() []pdf.TableHeaderColumn {
	var ha []pdf.TableHeaderColumn
	ha = append(ha,
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"日期"},
				Width:  55,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"傳票號碼"},
				Width:  80,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"會計項目與名稱"},
				Width:  120,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"客供商代號"},
				Width:  80,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"客戶/供應商"},
				Width:  100,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"摘要"},
				Width:  150,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"借方金額"},
				Width:  80,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"貸方金額"},
				Width:  80,
				Height: 20,
			}},
	)
	return ha
}

func amountFormat(amount int) string {
	ac := accounting.Accounting{Symbol: "", Precision: 0}
	return ac.FormatMoney(amount)
}
