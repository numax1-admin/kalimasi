package doc

import (
	"fmt"
	"kalimasi/util"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_getID(t *testing.T) {
	c := Company{
		UnitCode: "28862112",
	}
	oid := c.newID()
	fmt.Println(oid)
	unitCode := "F127032600"
	unitCode = util.StrAppend(idTransMap[unitCode[0]], unitCode[1:])
	fmt.Println(unitCode)
	i, err := strconv.Atoi(unitCode)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(i)

	fmt.Println(strings.HasPrefix("110222", "1102"))
	// at := AccTermList{
	// 	CompanyID: oid,
	// 	Typ:       TypeAccTermAssets,
	// }
	// id, err := at.getID()
	// fmt.Println(id, err)
	assert.True(t, false)
}

func Test_getReportID(t *testing.T) {
	c := Company{
		UnitCode: "022144444",
	}
	oid := c.newID()
	fmt.Println(oid)
	at := Report{
		CompanyID: oid,
		Typ:       "balanceBudget",
	}
	id := at.newID()
	fmt.Println(id)
	assert.True(t, false)
}
