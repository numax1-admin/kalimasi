package doc

import (
	"encoding/binary"
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
	"github.com/google/uuid"
)

const (
	companyC = "company"

	CompanyTypEnterprise = "enterprise"
	CompanyTypOrg        = "org"
)

type AccMapping struct {
	Name        string
	Display     string
	AccTermCode string
	Enable      bool
}

type CompanyTaxInfo struct {
	Code string // 稅籍編號
}

type AddressInfo struct {
	PostalCode string `json:"postalCode"`
	Country    string
	City       string
	Address    string
}

type ContactPersonInfo struct {
	Name string
	ContactInfo
}

type Company struct {
	ID        bson.ObjectId `bson:"_id"`
	Typ       string
	Name      string
	UnitCode  string // 統一編號
	TaxRate   int    // 稅率
	IsFake    bool   // 是否假的資料
	Fax       string
	OwnerName string
	AddressInfo
	ContactPersonInfo

	TaxInfo CompanyTaxInfo

	PayMethod []*AccMapping
	Incomes   []*AccMapping

	AuditSetting `bson:"auditsetting"`
	CompanyPerm  []*UserCompanyPerm `bson:"lookupUCP,omitempty"`
	// 憑證收件人清單
	VoucherRecipient []bson.ObjectId

	CommonDoc `bson:"meta"`
	AuditID
}

func IsFakeCompany(companyID bson.ObjectId) bool {
	data := binary.BigEndian.Uint64([]byte(companyID))
	r := data & (1 << 30)
	return r > 0
}

func GetCompanyId(unitCode string, isFake bool) (bson.ObjectId, error) {
	var b [12]byte
	i, err := strconv.ParseUint(unitCode, 10, 64)
	if err != nil {
		return "", errors.New("invalid unitCode")
	}
	if isFake {
		i = (1 << 30) + i
	}
	bs := make([]byte, 8)
	binary.BigEndian.PutUint64(bs, i)
	for i = 0; i < 8; i++ {
		b[i] = bs[i]
	}
	return bson.ObjectId(b[:]), nil
}

func (d *Company) newID() bson.ObjectId {
	id, err := GetCompanyId(d.UnitCode, d.IsFake)
	if err != nil {
		panic(err)
	}
	return id
}

func (u *Company) GetDoc() interface{} {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u
}

func (u *Company) GetC() string {
	return companyC
}

func (u *Company) GetID() bson.ObjectId {
	return u.ID
}

func (u *Company) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *Company) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *Company) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (u *Company) GetIncome(name string) (*AccMapping, bool) {
	return findAccMapping(name, u.Incomes)
}

func (u *Company) GetPayMethod(name string) (*AccMapping, bool) {
	return findAccMapping(name, u.PayMethod)
}

func findAccMapping(name string, list []*AccMapping) (*AccMapping, bool) {
	for _, m := range list {
		if m.Name == name {
			return &AccMapping{
				Name:        m.Name,
				Display:     m.Display,
				AccTermCode: m.AccTermCode,
			}, true
		}
	}
	return nil, false
}

func (u *Company) GetUpdateField() bson.M {
	return bson.M{
		"paymethod": u.PayMethod,
		"incomes":   u.Incomes,
	}
}

func (u *Company) GetPipeline(q bson.M) PipelineQryInter {
	return &CommonPipeline{
		q: q,
		pipeline: []bson.M{
			{"$match": q},
			{
				"$lookup": bson.M{
					"from":         userCompanyC,
					"localField":   "_id",
					"foreignField": "companyid",
					"as":           "lookupUCP",
				},
			},
		},
	}
}

func (u *Company) AddPayMethod(accM *AccMapping) error {
	//同一會計科目，應該可以有兩種以上付款方式
	//name way is declared [method-{timestamp}]
	reqID := uuid.New()
	key := reqID.String()
	accM.Name = fmt.Sprintf("method-%s", key)
	u.PayMethod = append(u.PayMethod, accM)
	return nil
}

func (u *Company) ChangePayMethod(accM *AccMapping) error {
	//if find same data then do update pay method
	for i, t := range u.PayMethod {
		if t.Name == accM.Name {
			u.PayMethod[i] = accM
			return nil
		}
	}
	return errors.New("[name]:pay method is not exist")
}

func (u *Company) DeletePayMethod(methodName string) error {
	tmp := []*AccMapping{}
	f := false
	//if find same data then do update pay method
	for _, t := range u.PayMethod {
		if t.Name == methodName {
			f = true
		} else {
			tmp = append(tmp, t)
		}
	}
	if !f {
		return errors.New("[name]:pay method is not exist")
	} else {
		u.PayMethod = tmp
		return nil
	}
}

func (u *Company) AddIncome(accM *AccMapping) error {
	//name way is declared [pay-{uuid}]
	reqID := uuid.New()
	key := reqID.String()
	accM.Name = fmt.Sprintf("pay-%s", key)
	u.Incomes = append(u.Incomes, accM)
	return nil
}

func (u *Company) ChangeIncome(accM *AccMapping) error {
	//if find same data then do update pay method
	for i, t := range u.Incomes {
		if t.Name == accM.Name {
			u.Incomes[i] = accM
			return nil
		}
	}
	return errors.New("[name]:pay item is not exist")
}

func (u *Company) DeleteIncome(incomeName string) error {
	tmp := []*AccMapping{}
	f := false
	//if find same data then do update pay method
	for _, t := range u.Incomes {
		if t.Name == incomeName {
			f = true
		} else {
			tmp = append(tmp, t)
		}
	}
	if !f {
		return errors.New("[name]:pay item is not exist")
	} else {
		u.Incomes = tmp
		return nil
	}
}

type NumaxSync struct {
	Company   `bson:"_id,inline"`
	NumaxSync struct {
		Auth     string
		Token    string
		Seller   string
		SyncTime time.Time
	}
}

func (u *NumaxSync) GetUpdateField() bson.M {
	return bson.M{
		"numaxsync": u.NumaxSync,
	}
}
