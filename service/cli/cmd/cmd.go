package cmd

import (
	"kalimasi/rsrc"
	"log"
	"os"

	"github.com/joho/godotenv"
	"github.com/urfave/cli"
)

type Cmd interface {
	Enable() bool
	GetCommands() []cli.Command
}

func GetCliApp(cmds ...Cmd) *cli.App {
	app := cli.NewApp()
	for _, cmd := range cmds {
		if cmd.Enable() {
			app.Commands = append(app.Commands, cmd.GetCommands()...)
		}
	}

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "configpath, c",
			Usage: "Load configuration from `FILE`",
		},
	}
	return app
}

func initDI(c *cli.Context) {
	confpath := c.GlobalString("configpath")
	err := godotenv.Load(confpath + ".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// [START setting_port]
	env := os.Getenv("ENV")
	if env == "" {
		env = "dev"
		log.Printf("Defaulting to ENV %s", env)
	}

	timezone := os.Getenv("TIMEZONE")
	if timezone == "" {
		timezone = "Asia/Taipei"
		log.Printf("Defaulting to timezone %s", timezone)
	}

	rsrc.IniConfByEnv(env, timezone)

}
