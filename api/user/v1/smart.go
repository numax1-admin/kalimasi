package v1

import (
	"encoding/json"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"
)

type SmartAPI string

func (api SmartAPI) GetName() string {
	return string(api)
}

func (a SmartAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		&rsrc.APIHandler{Path: "/v1/smart/candidate", Next: a.getCandidate, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/smart/candidate", Next: a.comFreUpdate, Method: "POST", Auth: true},
	}
}

func (a SmartAPI) Init() {
}

func (api *SmartAPI) getCandidate(w http.ResponseWriter, req *http.Request) {

	qv := util.GetQueryValue(req, []string{"vatNumber"}, true)
	vat := input.VatNumInput(qv["vatNumber"].(string))
	ui := input.GetUserInfo(req)

	if err := vat.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	di := rsrc.GetDI()
	client := di.GetSearch().GetClient()
	log := di.GetLog()
	mdb := di.GetMongoByReq(req)
	sm := model.NewSmartModel(mdb, client, log)

	fav, err := sm.GetMyFav(vat, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	result, err := sm.QueryCompany(req.Context(), vat)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	if result == nil && fav == "" {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	res := &model.ReturngetCandidate{
		Favorite:   fav,
		Candidates: result,
	}

	w.Header().Set("Content-Type", "application/json")
	if err = json.NewEncoder(w).Encode(res); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *SmartAPI) comFreUpdate(w http.ResponseWriter, req *http.Request) {

	qv := &input.ComUpdateInput{}
	err := json.NewDecoder(req.Body).Decode(qv)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	if err = qv.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	di := rsrc.GetDI()
	client := di.GetSearch().GetClient()
	log := di.GetLog()
	mdb := di.GetMongoByReq(req)
	sm := model.NewSmartModel(mdb, client, log)
	ui := input.GetUserInfo(req)

	err = sm.SaveMyFav(qv, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = sm.UpdateCandidate(req.Context(), qv)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

