package v1

import (
	"encoding/json"
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	inputreport "kalimasi/input/reportgener"
	"kalimasi/model"
	"kalimasi/model/reportgener"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"

	"github.com/globalsign/mgo/bson"
)

type CompanyAPI string

func (api CompanyAPI) GetName() string {
	return string(api)
}

func (a CompanyAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		{Path: "/v1/company", Next: a.createEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/company", Next: a.getEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/company", Next: a.putEndpoint, Method: "PUT", Auth: true},

		{Path: "/v1/company/conf", Next: a.getConfEndpoint, Method: "GET", Auth: true},

		{Path: "/v1/company/pay", Next: a.createPayMethodEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/company/{ID}/pay", Next: a.modifyPayMethodEndpoint, Method: "PUT", Auth: true},
		{Path: "/v1/company/{ID}/pay/{NAME}", Next: a.deletePayMethodEndpoint, Method: "DELETE", Auth: true},

		{Path: "/v1/company/income", Next: a.createIncomeEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/company/{ID}/income", Next: a.modifyIncomeEndpoint, Method: "PUT", Auth: true},
		{Path: "/v1/company/{ID}/income/{NAME}", Next: a.deleteIncomeEndpoint, Method: "DELETE", Auth: true},

		{Path: "/v1/company/budget", Next: a.getBudgetSettingEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/company/budget/{ID}", Next: a.modifyBudgetSettingEndpoint, Method: "PUT", Auth: true},

		{Path: "/v1/company/balanceSheet", Next: a.getBalanceSheetSettingEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/company/balanceSheet/{ID}", Next: a.modifyBalanceSheetSettingEndpoint, Method: "PUT", Auth: true},

		// 加入使用者公司權限
		{Path: "/v1/company/user", Next: a.createUserPermEndpoint, Method: "POST", Auth: true},
		// 使用者確認加入公司權限
		{Path: "/v1/company/user/confirm", Next: a.confirmUserPermEndpoint, Method: "POST", Auth: false},
		// 取得公司邀請信清單
		{Path: "/v1/company/invitation", Next: a.getInvitationEndpoint, Method: "GET", Auth: true},
		// 重寄使用者加入公司邀請信
		{Path: "/v1/company/invitation/{ID}", Next: a.resendUserPermEndpoint, Method: "PUT", Auth: true},
		// 取消使用者加入公司邀請信
		{Path: "/v1/company/invitation/{ID}", Next: a.delUserPermEndpoint, Method: "DELETE", Auth: true},
		// 取得公司使用者
		{Path: "/v1/company/user", Next: a.getCompanyUserEndpoint, Method: "GET", Auth: true},
		// 移除公司使用者
		{Path: "/v1/company/user/{UID}", Next: a.delUserEndpoint, Method: "DELETE", Auth: true},
		// 修改使用者權限
		{Path: "/v1/company/user/{UID}/perm", Next: a.updateUserPermEndpoint, Method: "PUT", Auth: true},
		// 修改公司使用者備註說明
		{Path: "/v1/company/user/{UID}/note", Next: a.updateUserNoteEndpoint, Method: "PUT", Auth: true},
		// 設定公司同步Token
		{Path: "/v1/company/e-invoice", Next: a.upserteInvoiceEndpoint, Method: "PUT", Auth: true},
		// 修改帳單收件人
		{Path: "/v1/company/voucher/recipient", Next: a.updateCompanyVoucherRecipientEndpoint, Method: "PUT", Auth: true},
	}
}

func (a CompanyAPI) Init() {

}

func (api *CompanyAPI) delUserPermEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	mongo := model.GetMgoDBModel(dbclt)
	i := &doc.Invitation{
		ID: qid,
	}
	err = mongo.FindByID(i)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}

	err = mongo.RemoveByID(i)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Write([]byte("ok"))
}

func (api *CompanyAPI) resendUserPermEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	input := map[string]interface{}{}
	err = json.NewDecoder(req.Body).Decode(&input)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	email, ok := input["email"].(string)
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("missing email"))
		return
	}
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	bm := model.GetCompanyModel(dbclt)
	mailServ := di.GetMailServ()
	if err = bm.ResendInvitation(mailServ, qid, email); err != nil {
		if apierr, ok := err.(rsrc.ApiError); ok {
			w.WriteHeader(apierr.StatusCode)
			w.Write([]byte(err.Error()))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (api *CompanyAPI) getInvitationEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)
	c := &doc.Invitation{}
	mgoDB := model.GetMgoDBModelByReq(req)
	result, err := mgoDB.Find(c, bson.M{"companyid": ui.GetCompany(), "typ": "inviteComUser"}, 0, 0)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}
	out, count := doc.Format(result, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Invitation); ok {
			return map[string]interface{}{
				"id":    d.ID.Hex(),
				"email": d.Email,
				"perm":  d.Parameter["perm"].(string),
			}
		}
		return nil
	})

	if count == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

}

func (api *CompanyAPI) updateCompanyVoucherRecipientEndpoint(w http.ResponseWriter, req *http.Request) {
	var uids []bson.ObjectId
	err := json.NewDecoder(req.Body).Decode(&uids)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	c := &doc.Company{
		ID: ui.CompanyID,
	}
	pipe := c.GetPipeline(bson.M{"_id": ui.CompanyID})
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.PipelineOne(c, pipe.GetPipeline())
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}
	var exist bool
	for _, uid := range uids {
		exist = false
		for _, up := range c.CompanyPerm {
			if uid == up.UserID {
				exist = true
				break
			}
		}
		if !exist {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("user id not exist in company permission: " + uid.Hex()))
			return
		}
	}
	err = mgoDB.UpdateV2(c, bson.M{"voucherrecipient": uids}, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *CompanyAPI) upserteInvoiceEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)
	c := &doc.Company{ID: ui.CompanyID}

	mgoDB := model.GetMgoDBModelByReq(req)
	err := mgoDB.FindByID(c)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}

	i := input.PutEInvoice{}
	err = json.NewDecoder(req.Body).Decode(&i)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	if err = i.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	token, err := rsrc.GetDI().NumaxParseToken(i.Token)

	if !doc.IsFakeCompany(ui.CompanyID) && !token.CheckUnitCode(c.UnitCode) {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("token not match company"))
		return
	}

	ns := &doc.NumaxSync{
		Company: *c,
	}
	ns.NumaxSync.Auth = token.GetAuth()
	ns.NumaxSync.Token = i.Token
	ns.NumaxSync.Seller = token.GetSeller()
	if err = mgoDB.Update(ns, ui); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *CompanyAPI) getCompanyUserEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)
	dbmodel := model.GetMgoDBModelByReq(req)
	c := &doc.Company{}
	pl := c.GetPipeline(bson.M{"_id": ui.CompanyID})
	err := dbmodel.PipelineOne(c, pl.GetPipeline())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	var userIDs []bson.ObjectId
	for _, cp := range c.CompanyPerm {
		userIDs = append(userIDs, cp.UserID)
	}
	u := &doc.UserCompanyPerm{}
	upl := u.GetPipeline(bson.M{"userid": bson.M{"$in": userIDs}, "companyid": ui.CompanyID})
	result, err := dbmodel.PipelineAll(u, upl.GetPipeline())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	out, count := doc.Format(result, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.UserCompanyPerm); ok {
			return map[string]interface{}{
				"id":    d.UserInfo[0].ID.Hex(),
				"name":  d.UserInfo[0].DisplayName,
				"phone": d.UserInfo[0].GetPhone(),
				"email": d.UserInfo[0].GetAcc(),
				"perm":  d.Permission,
				"note":  d.Note,
			}
		}
		return nil
	})

	if count == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *CompanyAPI) confirmUserPermEndpoint(w http.ResponseWriter, req *http.Request) {
	input := input.ConfirmCompanyPerm{}
	err := json.NewDecoder(req.Body).Decode(&input)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	if err = input.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	oid, err := model.GetInviteIDFromToken(input.Token)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid token"))
		return
	}
	di := rsrc.GetDI()
	mailServ := di.GetMailServ()
	log := di.GetLog()
	dbclt := di.GetMongoByReq(req)
	im := model.NewInvitationModel(dbclt, mailServ, log)
	if err = im.Response(oid, input.Response, model.NewInviteCompanyUserHandler(dbclt)); err != nil {
		if apierr, ok := err.(rsrc.ApiError); ok {
			w.WriteHeader(apierr.StatusCode)
			w.Write([]byte(err.Error()))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}
	w.WriteHeader(http.StatusOK)

}

func (api *CompanyAPI) createUserPermEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)
	di := rsrc.GetDI()
	input := input.CreateCompanyUserPerm{
		CompanyID:  ui.CompanyID,
		ConfirmURL: di.GetRedirectUrl("confirmPerm"),
	}
	err := json.NewDecoder(req.Body).Decode(&input)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	dbclt := di.GetMongoByReq(req)
	bm := model.GetCompanyModel(dbclt)
	mailServ := di.GetMailServ()
	if err = bm.InviteUser(mailServ, input, ui); err != nil {
		if apierr, ok := err.(rsrc.ApiError); ok {
			w.WriteHeader(apierr.StatusCode)
			w.Write([]byte(err.Error()))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (api *CompanyAPI) updateUserNoteEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"UID"})
	ui := input.GetUserInfo(req)
	if ui.Perm != doc.UserPermOwn {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	pcun := input.PutCompanyUserNote{
		CompanyID: ui.CompanyID,
		UserID:    vars["UID"].(string),
	}
	err := json.NewDecoder(req.Body).Decode(&pcun)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = pcun.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetCompanyModel(dbclt)
	if err := bm.ChangeCompUserNote(pcun, ui); err != nil {
		if apierr, ok := err.(rsrc.ApiError); ok {
			w.WriteHeader(apierr.StatusCode)
			w.Write([]byte(err.Error()))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}
	w.Write([]byte("ok"))
}

// 修改權限
func (api *CompanyAPI) updateUserPermEndpoint(w http.ResponseWriter, req *http.Request) {
	pcup := input.PutCompanyUserPerm{}
	err := json.NewDecoder(req.Body).Decode(&pcup)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	vars := util.GetPathVars(req, []string{"UID"})
	pcup.CompanyID = ui.CompanyID
	pcup.UserID = vars["UID"].(string)
	if err = pcup.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if ui.Perm != doc.UserPermOwn {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetCompanyModel(dbclt)
	if err := bm.ChangeCompUserPerm(pcup, ui); err != nil {
		if apierr, ok := err.(rsrc.ApiError); ok {
			w.WriteHeader(apierr.StatusCode)
			w.Write([]byte(err.Error()))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}
	w.Write([]byte("ok"))
}

func (api *CompanyAPI) delUserEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"UID"})
	uID := vars["UID"].(string)

	if !bson.IsObjectIdHex(uID) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid UID"))
		return
	}

	ui := input.GetUserInfo(req)
	if ui.Perm != doc.UserPermOwn {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetCompanyModel(dbclt)
	if err := bm.DeleteCompanyUser(uID, ui); err != nil {
		if apierr, ok := err.(rsrc.ApiError); ok {
			w.WriteHeader(apierr.StatusCode)
			w.Write([]byte(err.Error()))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}
	w.Write([]byte("ok"))
}

func (api *CompanyAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.CreateCompany{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetCompanyModel(dbclt)

	if bm.IsCompanyExist(cb.VATnumber) {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte("company is exist"))
		return
	}

	// 是否要系統審核
	if model.SystemAudit(cb, w, req) {
		return
	}

	// 不需審核 or 審核通過直接建立
	err = bm.Create(cb, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *CompanyAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)

	c := &doc.Company{ID: ui.CompanyID}

	mgoDB := model.GetMgoDBModelByReq(req)
	pipe := c.GetPipeline(bson.M{"_id": ui.CompanyID})
	err := mgoDB.PipelineOne(c, pipe.GetPipeline())
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}
	var oid bson.ObjectId
	for _, up := range c.CompanyPerm {
		if up.Permission == doc.UserPermOwn {
			oid = up.UserID
		}
	}

	user := &doc.User{ID: oid}
	err = mgoDB.FindByID(user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("can not find owner: " + err.Error()))
		return
	}
	contact := user.GetContractRecord(0)
	var vr []map[string]interface{}
	if len(c.VoucherRecipient) == 0 {
		vr = append(vr, map[string]interface{}{
			"id":    user.ID.Hex(),
			"email": user.Email,
			"contact": map[string]interface{}{
				"ctType": contact.Typ,
				"value":  contact.Value,
			},
		})
	} else {
		userlist, err := mgoDB.Find(user, bson.M{"_id": bson.M{"$in": c.VoucherRecipient}}, 0, 0)
		if err != nil {
			vr = append(vr, map[string]interface{}{
				"id":    user.ID.Hex(),
				"email": user.Email,
				"contact": map[string]interface{}{
					"ctType": contact.Typ,
					"value":  contact.Value,
				},
			})
		}
		userAry := userlist.([]*doc.User)
		for _, u := range userAry {
			userCont := user.GetContractRecord(0)
			vr = append(vr, map[string]interface{}{
				"id":    u.ID.Hex(),
				"email": u.Email,
				"contact": map[string]interface{}{
					"ctType": userCont.Typ,
					"value":  userCont.Value,
				},
			})
		}
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"type":      c.Typ,
		"vatNumber": c.UnitCode,
		"name":      c.Name,
		"addressInfo": map[string]interface{}{
			"postalCode": c.AddressInfo.PostalCode,
			"country":    c.AddressInfo.Country,
			"city":       c.AddressInfo.City,
			"address":    c.AddressInfo.Address,
		},
		"fax":     c.Fax,
		"owner":   c.OwnerName,
		"taxCode": c.TaxInfo.Code,
		"ownUser": map[string]interface{}{
			"id":    user.ID.Hex(),
			"email": user.Email,
			"name":  user.DisplayName,
			"contact": map[string]interface{}{
				"ctType": contact.Typ,
				"value":  contact.Value,
			},
		},
		"voucherRecipient": vr,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *CompanyAPI) putEndpoint(w http.ResponseWriter, req *http.Request) {
	ipc := input.PutCompany{}
	err := json.NewDecoder(req.Body).Decode(&ipc)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = ipc.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)

	c := &doc.Company{ID: ui.CompanyID}

	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(c)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}

	err = mgoDB.UpdateV2(c, bson.M{
		"name":        ipc.Name,
		"fax":         ipc.Fax,
		"ownername":   ipc.Owner,
		"taxinfo":     doc.CompanyTaxInfo{Code: ipc.TaxCode},
		"addressinfo": ipc.AddressInfo,
	}, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (api *CompanyAPI) getConfEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"usage"}, true)
	isEdit := (qv["usage"].(string) == "edit")
	ui := input.GetUserInfo(req)

	c := &doc.Company{ID: ui.CompanyID}

	mgoDB := model.GetMgoDBModelByReq(req)
	err := mgoDB.FindByID(c)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}

	di := rsrc.GetDI()
	mgo := di.GetMongoByReq(req)
	atm := model.GetAccTermModel(mgo)
	atlpay := atm.FindAccTermList(ui.CompanyID, doc.TypeAccTermAssets)
	var t *doc.AccTerm
	payMethod, income := []map[string]interface{}{}, []map[string]interface{}{}
	for _, p := range c.PayMethod {
		t = atlpay.GetAccTerm(p.AccTermCode)
		if t == nil {
			continue
		}
		if isEdit || p.Enable {
			payMethod = append(payMethod, map[string]interface{}{
				"name":        p.Name,
				"display":     p.Display,
				"enable":      p.Enable,
				"accTermCode": p.AccTermCode,
				"accTermName": t.Name,
			})
		}
	}

	atlincome := atm.FindAccTermList(ui.CompanyID, doc.TypeAccTermIncome)
	for _, i := range c.Incomes {
		t = atlincome.GetAccTerm(i.AccTermCode)
		if t == nil {
			if t = atlpay.GetAccTerm(i.AccTermCode); t == nil {
				continue
			}
		}
		if isEdit || i.Enable {
			income = append(income, map[string]interface{}{
				"name":        i.Name,
				"display":     i.Display,
				"enable":      i.Enable,
				"accTermCode": i.AccTermCode,
				"accTermName": t.Name,
			})
		}
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"payMethod": payMethod,
		"incomes":   income,
		"taxRate":   c.TaxRate,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *CompanyAPI) createPayMethodEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &input.PayAccTermMapConf{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	cm := model.GetCompanyModel(dbclt)
	err = cm.CreatePayMethod(ca, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) createIncomeEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &input.PayAccTermMapConf{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	di := rsrc.GetDI()
	di.Log.Warn(fmt.Sprintln(ca.Display, ca.AccTermCode))

	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	cm := model.GetCompanyModel(dbclt)
	err = cm.CreateIncome(ca, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) modifyPayMethodEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &input.PutPayAccTermMapConf{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)

	if err != nil || qid != ca.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	cm := model.GetCompanyModel(dbclt)
	err = cm.ModifyPayMethod(ca, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) deletePayMethodEndpoint(w http.ResponseWriter, req *http.Request) {

	vars := util.GetPathVars(req, []string{"ID", "NAME"})
	queryID := vars["ID"]
	NAME := vars["NAME"].(string)
	qid, err := doc.GetObjectID(queryID)
	ui := input.GetUserInfo(req)

	if err != nil || qid != ui.CompanyID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	dbclt := rsrc.GetDI().GetMongoByReq(req)
	cm := model.GetCompanyModel(dbclt)
	err = cm.DeletePayMethod(NAME, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) modifyIncomeEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &input.PutPayAccTermMapConf{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)

	if err != nil || qid != ca.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	cm := model.GetCompanyModel(dbclt)
	err = cm.ModifyIncome(ca, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) deleteIncomeEndpoint(w http.ResponseWriter, req *http.Request) {

	vars := util.GetPathVars(req, []string{"ID", "NAME"})
	queryID := vars["ID"]
	NAME := vars["NAME"].(string)
	qid, err := doc.GetObjectID(queryID)
	ui := input.GetUserInfo(req)

	if err != nil || qid != ui.CompanyID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	dbclt := rsrc.GetDI().GetMongoByReq(req)
	cm := model.GetCompanyModel(dbclt)
	err = cm.DeleteIncome(NAME, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) getBudgetSettingEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)

	c := &doc.Report{ID: doc.GetReportID(doc.BalanceBudget, ui.CompanyID)}
	mgoDB := model.GetMgoDBModelByReq(req)
	err := mgoDB.FindByID(c)
	if err != nil {
		dbclt := rsrc.GetDI().GetMongoByReq(req)
		rm := model.GetReportModel(dbclt)
		r := &reportgener.BalanceBudget{CompanyID: ui.CompanyID}
		rm.SaveSetting(r, ui)

		err := mgoDB.FindByID(c)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(err.Error()))
			return
		}
	}

	sbb := []*reportgener.SettingBalanceBudget{}
	err = json.Unmarshal([]byte(c.JsonSetting), &sbb)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	if sbb != nil {
		for _, p1 := range sbb {
			p1.SetEmpty()
		}
	} else {
		sbb = []*reportgener.SettingBalanceBudget{}
	}

	w.Header().Set("Content-Type", "application/json")

	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"id":                   c.GetID(),
		"settingBalanceBudget": sbb,
	})

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *CompanyAPI) modifyBudgetSettingEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &inputreport.PutSettingBalanceBudget{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)

	ui := input.GetUserInfo(req)
	rid := doc.GetReportID(doc.BalanceBudget, ui.CompanyID)

	if err != nil || qid != ca.ID || rid != ca.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	dbclt := rsrc.GetDI().GetMongoByReq(req)
	rm := model.GetReportModel(dbclt)
	err = rm.ModifyReportBalanceBudget(ca, ui)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) getBalanceSheetSettingEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)
	c := &doc.Report{ID: doc.GetReportID(doc.BalanceSheet, ui.CompanyID)}
	mgoDB := model.GetMgoDBModelByReq(req)
	err := mgoDB.FindByID(c)
	if err != nil {

		dbclt := rsrc.GetDI().GetMongoByReq(req)
		rm := model.GetReportModel(dbclt)
		r := &reportgener.BalanceSheet{CompanyID: ui.CompanyID}
		rm.SaveSetting(r, ui)

		err := mgoDB.FindByID(c)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(err.Error()))
			return
		}
	}

	sbb := reportgener.BalanceSheet{}
	err = json.Unmarshal([]byte(c.JsonSetting), &sbb)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	sbb.SetEmpty()

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"id":                  c.GetID(),
		"settingBalanceSheet": sbb,
	})

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *CompanyAPI) modifyBalanceSheetSettingEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &inputreport.PutSettingBalanceSheet{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)

	ui := input.GetUserInfo(req)
	rid := doc.GetReportID(doc.BalanceSheet, ui.CompanyID)

	if err != nil || qid != ca.ID || rid != ca.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	dbclt := rsrc.GetDI().GetMongoByReq(req)
	rm := model.GetReportModel(dbclt)
	err = rm.ModifyReportBalanceSheet(ca, ui)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}
