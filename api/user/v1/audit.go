package v1

import (
	"encoding/json"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"
	"time"
)

type AuditAPI string

func (api AuditAPI) GetName() string {
	return string(api)
}

func (a AuditAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		{Path: "/v1/audit", Next: a.getEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/audit/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/audit/{ID}", Next: a.modifyEndpoint, Method: "PUT", Auth: true},

		// 公司審核權限管理
		{Path: "/v1/company/audit", Next: a.getCompanyAuditEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/company/audit", Next: a.setCompanyAuditEndpoint, Method: "PUT", Auth: true},
	}
}

func (a AuditAPI) Init() {

}
func (api *AuditAPI) getCompanyAuditEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)

	c := &doc.Company{ID: ui.CompanyID}

	mgoDB := model.GetMgoDBModelByReq(req)
	err := mgoDB.FindByID(c)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}

	ap := []map[string]interface{}{}
	for _, p := range c.AuditSetting.AuditProcess {
		ap = append(ap, map[string]interface{}{
			"type":  p.Typ,
			"value": p.Value,
		})
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"isAudit":      c.AuditSetting.IsAudit,
		"auditProcess": ap,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *AuditAPI) setCompanyAuditEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.UpdateCompanyAudit{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	ca := cb.ToDoc()
	ca.SetCompany(ui.CompanyID)

	dbm := model.GetMgoDBModelByReq(req)
	err = dbm.Update(ca, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *AuditAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"t", "s"}, true)
	ui := input.GetUserInfo(req)
	qb := input.QueryAudit{
		Typ:       qv["t"].(string),
		State:     qv["s"].(string),
		CompanyID: ui.CompanyID,
	}
	if err := qb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	q := qb.GetMgoQuery()
	mgodb := model.GetMgoDBModelByReq(req)
	a := &doc.Audit{
		CompanyID: ui.CompanyID,
	}
	result, err := mgodb.Find(a, q, 0, 0)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	out, c := doc.Format(result, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Audit); ok {
			ass := map[string]interface{}{}
			if d.Assessor == nil {
				ass["id"] = ""
				ass["name"] = ""
			} else {
				ass["id"] = d.Assessor.ID.Hex()
				ass["name"] = d.Assessor.Name
			}
			if d.CanApply(ui.ID, ui.Perm) {
				d.State = doc.AuditStateNew
			}
			return map[string]interface{}{
				"id":    d.ID.Hex(),
				"state": d.State,
				"meta":  d.Summary,
				"applicant": map[string]interface{}{
					"id":   d.Applicant.ID.Hex(),
					"name": d.Applicant.Name,
				},
				"assessor":   ass,
				"createDate": d.CreateTime.Format(time.RFC3339),
			}
		}
		return nil
	})

	if c == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *AuditAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)

	a := &doc.Audit{ID: qid, CompanyID: ui.CompanyID}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(a)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}

	di := rsrc.GetDI()
	out, _ := doc.Format(a, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Audit); ok {
			ass := map[string]interface{}{}
			if d.Assessor == nil {
				ass["id"] = ""
				ass["name"] = ""
			} else {
				ass["id"] = d.Assessor.ID.Hex()
				ass["name"] = d.Assessor.Name
			}
			data := map[string]interface{}{}
			err := json.Unmarshal(d.SerialJSON, &data)
			if err != nil {
				di.GetLog().Err(err.Error())
			}
			if d.CanApply(ui.ID, ui.Perm) {
				d.State = doc.AuditStateNew
			}

			return map[string]interface{}{
				"id":    d.ID.Hex(),
				"state": d.State,
				"data":  data,
				"applicant": map[string]interface{}{
					"id":   d.Applicant.ID.Hex(),
					"name": d.Applicant.Name,
				},
				"assessor":   ass,
				"createDate": d.CreateTime.Format(time.RFC3339),
				"updateDate": d.UpdateDate.Format(time.RFC3339),
				"records":    d.Records,
				"canApply":   d.CanApply(ui.ID, ui.Perm),
			}
		}
		return nil
	})
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *AuditAPI) modifyEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)

	ua := &input.UpdateAudit{}
	err = json.NewDecoder(req.Body).Decode(ua)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = ua.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	am := model.GetAuditModel(dbclt)
	if err = am.Apply(qid, ua, ui); err != nil {
		if apierr, ok := err.(rsrc.ApiError); ok {
			w.WriteHeader(apierr.StatusCode)
			w.Write([]byte(err.Error()))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
	} else {
		w.Write([]byte("ok"))
	}
}
