package v1

import (
	"encoding/json"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"

	"github.com/globalsign/mgo/bson"
)

type AuthAPI string

func (api AuthAPI) GetName() string {
	return string(api)
}

func (a AuthAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		{Path: "/v1/app", Next: a.appEndpoint, Method: "GET", Auth: false},
		{Path: "/v1/login", Next: a.loginEndpoint, Method: "POST", Auth: false},
		{Path: "/v1/token", Next: a.getTokenEndpoint, Method: "GET", Auth: false},
		{Path: "/v1/signup", Next: a.signupEndpoint, Method: "POST", Auth: false},
		{Path: "/v1/user/company", Next: a.userCompanyEndpoint, Method: "GET", Auth: true},
	}
}

func (a AuthAPI) Init() {

}

// firebase token to self token
func (api *AuthAPI) getTokenEndpoint(w http.ResponseWriter, req *http.Request) {
	ftoken := req.Header.Get("Fb-Token")
	if ftoken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	mgodb := rsrc.GetDI().GetMongoByReq(req)
	fsdb := rsrc.GetDI().GetFirebaseByReq(req)
	um := model.GetUserModelV3(mgodb, fsdb)

	t, s, err := um.VerifyFBToken(ftoken)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"token": t,
		"state": s,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *AuthAPI) signupEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.SignUp{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	mgodb := rsrc.GetDI().GetMongoByReq(req)
	fsdb := rsrc.GetDI().GetFirebaseByReq(req)
	um := model.GetUserModelV3(mgodb, fsdb)
	_, err = um.SignUp(cb)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte("ok"))
}

func (api *AuthAPI) loginEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.LoginWithCompany{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	cb.ClientInfo = util.GetClientInfo(req)
	mgodb := rsrc.GetDI().GetMongoByReq(req)
	um := model.GetUserModel(mgodb)
	u, p, s, err := um.Signin(cb)
	if err != nil {
		switch err.Error() {
		case "company not found":
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte(err.Error()))
		case "not found":
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("account or password error"))
		default:
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}

	c := &doc.Company{}
	mgoModel := model.GetMgoDBModel(mgodb)
	err = mgoModel.FindOne(c, bson.M{"unitcode": cb.Company})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	payMethod, income := []map[string]interface{}{}, []map[string]interface{}{}
	for _, p := range c.PayMethod {
		payMethod = append(payMethod, map[string]interface{}{
			"name":    p.Name,
			"display": p.Display,
		})
	}

	for _, i := range c.Incomes {
		income = append(income, map[string]interface{}{
			"name":    i.Name,
			"display": i.Display,
		})
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"token":      u,
		"permission": p,
		"state":      s,
		"payMethod":  payMethod,
		"incomes":    income,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *AuthAPI) appEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"p"}, true)
	platform := qv["p"].(string)

	switch platform {
	case "vb":
		w.Write([]byte("OK"))
	default:
		w.WriteHeader(http.StatusForbidden)
	}

}

func (api *AuthAPI) userCompanyEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)
	if !bson.IsObjectIdHex(ui.ID) {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	uid := bson.ObjectIdHex(ui.ID)

	c := &doc.UserCompanyPerm{}
	mgodb := rsrc.GetDI().GetMongoByReq(req)
	mgoModel := model.GetMgoDBModel(mgodb)
	r, err := mgoModel.Find(c, bson.M{"userid": uid}, 0, 0)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	ucp := r.([]*doc.UserCompanyPerm)
	l := len(ucp)
	comlist := make([]bson.ObjectId, l)
	permMap := make(map[string]string)
	for i := 0; i < l; i++ {
		comlist[i] = ucp[i].CompanyID
		permMap[ucp[i].CompanyID.Hex()] = ucp[i].Permission
	}

	com := &doc.Company{}
	clist, err := mgoModel.Find(com, bson.M{"_id": bson.M{"$in": comlist}}, 0, 0)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	cAry := clist.([]*doc.Company)
	out, _ := doc.Format(cAry, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Company); ok {
			if perm, okp := permMap[d.ID.Hex()]; okp {
				return map[string]interface{}{
					"id":         d.ID.Hex(),
					"name":       d.Name,
					"unitCode":   d.UnitCode,
					"isFake":     d.IsFake,
					"isAudit":    false,
					"auditState": "",
					"permission": perm,
				}
			}
		}
		return nil
	})

	// 取得審核中的公司清單
	am := model.GetAuditModel(mgodb)
	aslist := am.GetSysAuditList(uid)
	result := out.([]map[string]interface{})
	for _, as := range aslist {
		result = append(result, map[string]interface{}{
			"id":         "",
			"name":       as.Summary["name"],
			"unitCode":   as.Summary["vatNumber"],
			"isAudit":    true,
			"auditState": as.State,
			"isFake":     false,
			"perm":       doc.UserPermOwn,
		})
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(result)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}
