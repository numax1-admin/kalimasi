package model

import (
	"errors"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc"
	"kalimasi/rsrc/log"
	"kalimasi/util"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type userModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetUserModel(mgodb *mgo.Database) *userModel {
	mongo := GetMgoDBModel(mgodb)
	return &userModel{dbmodel: mongo, log: mongo.log}
}

func (bm *userModel) ServiceHasAcc(service string) bool {
	q := bson.M{"service": service}
	u := &doc.User{}
	c := bm.dbmodel.Count(u, q)
	return c > 0
}

func (bm *userModel) BasicSignin(l *input.BasicLogin) (token string, perm string, state string, err error) {
	cp := util.MD5(l.Pwd)
	q := bson.M{"email": l.Account, "pwd": cp, "service": l.Service}
	u := &doc.User{}
	err = bm.dbmodel.PipelineOne(u, u.GetPipeline(q).GetPipeline())
	if err != nil {
		return "", "", "", err
	}
	if !u.ID.Valid() {
		return "", "", "", errors.New("not found")
	}
	perm = doc.UserPermUser
	u.AddLoginRecord(l.ClientInfo)
	err = bm.dbmodel.Update(u, nil)
	if err != nil {
		return "", "", "", err
	}
	t, err := rsrc.GetDI().GetJWTConf().GetToken(
		map[string]interface{}{
			"sub": u.ID.Hex(),
			"acc": u.Email,
			"nam": u.DisplayName,
			"per": perm,
		})
	if err != nil {
		return "", "", "", err
	}
	return *t, perm, u.GetState(), nil
}

func (bm *userModel) Signin(l *input.LoginWithCompany) (token string, perm string, state string, err error) {
	cp := util.MD5(l.Pwd)
	q := bson.M{"email": l.Account, "pwd": cp}
	u := &doc.User{}
	err = bm.dbmodel.PipelineOne(u, u.GetPipeline(q).GetPipeline())
	if err != nil {
		return "", "", "", err
	}
	if !u.ID.Valid() {
		return "", "", "", errors.New("not found")
	}

	u.AddLoginRecord(l.ClientInfo)
	err = bm.dbmodel.Update(u, nil)
	if err != nil {
		return "", "", "", err
	}

	ucp, err := u.GetUserCompanyPerm(l.Company, l.IsFakeCompany)
	if err != nil {
		return "", "", "", err
	}
	t, err := rsrc.GetDI().GetJWTConf().GetToken(
		map[string]interface{}{
			"sub": u.ID.Hex(),
			"acc": u.Email,
			"nam": u.DisplayName,
			"per": ucp.Permission,
			"cat": ucp.CompanyID.Hex(),
		})
	if err != nil {
		return "", "", "", err
	}
	return *t, ucp.Permission, u.GetState(), nil
}

func (bm *userModel) SigninV2(l *input.BasicLogin) (token string, state string, err error) {
	cp := util.MD5(l.Pwd)
	q := bson.M{"email": l.Account, "pwd": cp}
	u := &doc.User{}
	err = bm.dbmodel.PipelineOne(u, u.GetPipeline(q).GetPipeline())
	if err != nil {
		return "", "", err
	}
	if !u.ID.Valid() {
		return "", "", errors.New("not found")
	}

	u.AddLoginRecord(l.ClientInfo)
	err = bm.dbmodel.Update(u, nil)
	if err != nil {
		return "", "", err
	}
	t, err := getTokenNoCompany(u.ID.Hex(), u.DisplayName, u.Email)
	if err != nil {
		return "", "", err
	}
	return t, u.GetState(), nil
}

func getTokenNoCompany(id, name, email string) (token string, err error) {
	t, err := rsrc.GetDI().GetJWTConf().GetToken(
		map[string]interface{}{
			"sub": id,
			"nam": name,
			"acc": email,
		})
	if err != nil {
		return "", err
	}
	return *t, nil
}
