package model

import (
	"context"
	"errors"
	"io"
	"kalimasi/doc"
	"kalimasi/input"
	inputreport "kalimasi/input/reportgener"
	"kalimasi/model/reportgener"
	"kalimasi/report"
	"kalimasi/rsrc"
	"kalimasi/rsrc/log"
	"net/url"
	"os"
	"time"

	"cloud.google.com/go/storage"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"google.golang.org/api/option"
)

const (
	ContentTypePDF = "application/pdf"
	ContentTypeTXT = "text/plain"

	_STORAGE_HOST = "storage-eaccount.numax.com.tw"
)

type reportModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetReportModel(mgodb *mgo.Database) *reportModel {
	mongo := GetMgoDBModel(mgodb)
	return &reportModel{dbmodel: mongo, log: mongo.log}
}

func (bm *reportModel) OutputJSON(si reportgener.ReportGenInter, w io.Writer) error {
	if !si.IsSetting() {
		return errors.New("not setting")
	}
	ri, err := si.GetReportObj(si.GetTitle(), nil)
	if err != nil {
		return err
	}
	return ri.Json(w)
}

func getGCPWriter(ctx context.Context, credentPath, filepath, contentType string) (*storage.Writer, error) {
	client, err := storage.NewClient(ctx, option.WithCredentialsFile(credentPath))
	if err != nil {
		return nil, err
	}
	bucket := os.Getenv("BUCKET")
	wc := client.Bucket(bucket).Object(filepath).NewWriter(ctx)
	wc.ContentType = contentType
	wc.ACL = []storage.ACLRule{{Entity: storage.AllUsers, Role: storage.RoleReader}}
	return wc, nil
}

func (bm *reportModel) GenerateTXT(si reportgener.ReportGenInter, u *input.ReqUser) (string, error) {
	if !si.IsSetting() {
		return "", errors.New("not setting")
	}
	di := rsrc.GetDI()
	ri, err := si.GetReportObj(si.GetTitle(), nil)
	if err != nil {
		return "", err
	}

	// 控制是否上傳到gcp storage
	testing := false
	if testing {
		f, _ := os.Create("./week_report.pdf")
		defer f.Close()
		return "", ri.PDF(f)
	}

	jsonPath := di.GetGCPCredentialPath()
	filepath := si.GetFileName()
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Second*50)
	defer cancel()
	wc, err := getGCPWriter(ctx, jsonPath, filepath, ContentTypeTXT)
	if err != nil {
		return "", err
	}
	if err = ri.TXT(wc); err != nil {
		return "", err
	}
	if err = wc.Close(); err != nil {
		return "", err
	}
	ul := url.URL{
		Scheme: "https",
		Host:   _STORAGE_HOST,
		Path:   wc.Attrs().Name,
	}
	return ul.String(), nil
}

func (bm *reportModel) GeneratePDF(si reportgener.ReportGenInter, u *input.ReqUser) (string, error) {
	if !si.IsSetting() {
		return "", errors.New("not setting")
	}
	di := rsrc.GetDI()
	font := di.GetFontMap()
	ri, err := si.GetReportObj(si.GetTitle(), font)
	if err != nil {
		return "", err
	}

	// 控制是否上傳到gcp storage
	testing := false
	if testing {
		f, _ := os.Create("./week_report.pdf")
		defer f.Close()
		return "", ri.PDF(f)
	}
	return bm.GeneratePDFByReport(si.GetFileName(), ri, u)
}

func (bm *reportModel) GeneratePDFByReport(filepath string, ri report.ReportInter, u *input.ReqUser) (string, error) {
	di := rsrc.GetDI()
	jsonPath := di.GetGCPCredentialPath()
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Second*50)
	defer cancel()
	wc, err := getGCPWriter(ctx, jsonPath, filepath, ContentTypePDF)
	if err = ri.PDF(wc); err != nil {
		return "", err
	}
	if err = wc.Close(); err != nil {
		return "", err
	}
	ul := url.URL{
		Scheme: "https",
		Host:   _STORAGE_HOST,
		Path:   wc.Attrs().Name,
	}
	return ul.String(), nil
}

func (bm *reportModel) LoadSetting(si reportgener.ReportGenInter) error {
	r := doc.Report{
		ID: si.GetReportID(),
	}
	err := bm.dbmodel.FindByID(&r)
	if err != nil {
		return err
	}

	err = si.Setting(r.JsonSetting)
	if err != nil {
		return err
	}
	return nil
}

func (bm *reportModel) SaveSetting(si reportgener.ReportGenInter, u *input.ReqUser) error {
	r := &doc.Report{
		Typ:         si.GetType(),
		JsonSetting: si.GetData(),
		CompanyID:   u.CompanyID,
		ID:          doc.GetReportID(si.GetType(), u.CompanyID),
	}

	return bm.dbmodel.Save(r, u)
}

func (bm *reportModel) ModifyReportBalanceBudget(pb *inputreport.PutSettingBalanceBudget, u *input.ReqUser) error {
	bs := &doc.Report{ID: doc.GetReportID(doc.BalanceBudget, pb.ID), CompanyID: u.GetCompany()}
	err := bm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}

	um := pb.GetReportDoc()
	um.ID = bs.ID
	um.CompanyID = u.GetCompany()

	err = bm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return bm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}

func (bm *reportModel) ModifyReportBalanceSheet(pb *inputreport.PutSettingBalanceSheet, u *input.ReqUser) error {
	bs := &doc.Report{ID: doc.GetReportID(doc.BalanceSheet, pb.ID), CompanyID: u.GetCompany()}
	err := bm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}

	um := pb.GetReportDoc()
	um.ID = bs.ID
	um.CompanyID = u.GetCompany()

	err = bm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return bm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}

func (bm *reportModel) GetReportBalanceSheetSetting(companyID bson.ObjectId, u *input.ReqUser) error {
	return nil
}
