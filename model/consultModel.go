package model

import (
	"strings"

	"kalimasi/doc"
	"kalimasi/input"

	"kalimasi/rsrc/log"
	"kalimasi/rsrc/mail"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type consultModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetConsultModel(mgodb *mgo.Database) *consultModel {
	mongo := GetMgoDBModel(mgodb)
	return &consultModel{
		dbmodel: mongo,
		log:     mongo.log,
	}
}

func (csm *consultModel) Create(input *input.CreateConsult, u *input.ReqUser) error {
	return csm.dbmodel.Save(input.ToDoc(), u)
}

func (csm *consultModel) IsConsultExist(csAccount string) bool {
	cs := doc.Consult{}
	bs := bson.M{"account": csAccount}

	_ = csm.dbmodel.FindOne(&cs, bs)
	if cs.Name != "" {
		return true
	}
	return false

}

func (csm *consultModel) InsertInviteMail(input *input.CreateInviteMail, u *input.ReqUser) error {
	return csm.dbmodel.Save(input.ToDoc(), u)
}

func (csm *consultModel) IsInviteMailExist(mail string) bool {
	cs := doc.InviteMail{}
	bs := bson.M{"mail": mail}

	_ = csm.dbmodel.FindOne(&cs, bs)
	if cs.Creator != "" {
		return true
	}
	return false

}

func (csm *consultModel) SendConsultInvitation(mailServ mail.MailServ, input *input.CreateInviteMail, u *input.ReqUser) error {

	//check if invitation send before
	i := &doc.Invitation{}
	q := bson.M{"email": input.Mail}
	q["typ"] = doc.InvitationTypeConsult
	err := csm.dbmodel.FindOne(i, q)
	if err != nil {
		slice := strings.Split(input.Mail, "@")
		parameter := map[string]interface{}{
			"email": input.Mail,
		}

		inviteID := bson.NewObjectId()

		data := struct {
			Name, Invitor, Company, URL string
		}{
			slice[0],
			u.GetName(),
			u.GetName(),
			"TEST_URL",
		}
		html, err := GetTplString(mailServ.GetHtmlTemplateFile("inviteConsult.html"), data)
		if err != nil {
			return err
		}
		plaint, err := GetTplString(mailServ.GetTextTemplateFile("inviteConsult.txt"), data)
		if err != nil {
			return err
		}

		i = &doc.Invitation{
			ID:        inviteID,
			Email:     input.Mail,
			Typ:       doc.InvitationTypeConsult,
			Parameter: parameter,
			Plaint:    plaint,
			Html:      html,
		}
		err = csm.dbmodel.Save(i, u)
		if err != nil {
			return err
		}
	}

	title := inviatationTitleMap[doc.InvitationTypeCompanyUser]
	err = mailServ.Subject(title).Html(i.Html).PlaintText(i.Plaint).SendSingle("", i.Email)
	if err != nil {
		return err
	}

	return nil

}
