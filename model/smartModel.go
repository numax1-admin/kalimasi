package model

import (
	"context"
	"encoding/json"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc/log"
	"kalimasi/util"

	"github.com/globalsign/mgo"
	elastic "github.com/olivere/elastic/v7"
)

const(
	size = 5
	esIndex = "vatcandidate"
)

type smartModel struct {
	
	dbmodel *mgoDBModel
	esclt *elastic.Client

	log *log.Logger
}

type ReturngetCandidate struct{
	Favorite  string     `json:"myFav"`
	Candidates   []input.ComInfo    `json:"candidates"`
}

func NewSmartModel(mgodb *mgo.Database, search *elastic.Client, log *log.Logger) *smartModel {
	if search == nil || log == nil || mgodb==nil{
		panic("missing paramter")
	}

	mongo := GetMgoDBModel(mgodb)
	return &smartModel{
		dbmodel:    mongo,
		esclt: search,
		log:   log,
	}
}

type comVatESDoc struct {
	ID        string `json:"ID"`
	VatNumber string `json:"vatNumber"`
	UploadFre int    `json:"uploadFre"`
	Name      string `json:"companyName"`
}

func (sm *smartModel) QueryCompany(ctx context.Context,vat input.VatNumInput) ([]input.ComInfo, error) {
	termQuery := elastic.NewTermQuery("vatNumber", string(vat))
	
	searchResult, err := sm.esclt.Search().
		Index(esIndex). 
		Query(termQuery).
		Sort("uploadFre", false).
		From(0).Size(size).
		Do(ctx)
	if err != nil {
		return nil, err
	}

	var result []input.ComInfo
	if searchResult.TotalHits() > 0 {
		for _, hit := range searchResult.Hits.Hits {
			var t comVatESDoc
			json.Unmarshal(hit.Source, &t)
			result = append(result, input.ComInfo{t.Name})
		}
		return result, nil
	}
	return nil, nil
}

func (sm *smartModel) UpdateCandidate(ctx context.Context,pi *input.ComUpdateInput) error {
	vat := pi.VatNumber
	name := pi.Company.Name
	id := util.MD5(vat + name)
		 
	if _, err := sm.esclt.Update().Index(esIndex).Id(id).
 		Script(elastic.NewScript("ctx._source.uploadFre += params.num").Param("num", 1)).
		Upsert(map[string]interface{}{"ID": id, "vatNumber": vat, "uploadFre": 1, "companyName": name}).
 		Do(ctx) ; err != nil{
		return err
	}
	return nil
}

func (sm *smartModel) GetMyFav(vat input.VatNumInput , ui *input.ReqUser)(string,error){
	Vat := string(vat)
	comID := ui.GetCompany()
	
	r := &doc.MyFav{
		VatNumber: Vat,
		CompanyID: comID,
	}
	r.GetID()

	err := sm.dbmodel.FindByID(r)
	if err != nil && (err.Error() == "not found") {
		return "" , nil
	}
	if  err != nil {
		return "" , err
	}
	
	return r.FavComName , nil
}

func (sm *smartModel) SaveMyFav(pi *input.ComUpdateInput , ui *input.ReqUser) error {
		
	comID := ui.GetCompany()
	favCom := string(pi.Company.Name)
    favVat := string(pi.VatNumber)

	mf := &doc.MyFav{
        CompanyID:comID,  
        FavComName:favCom,
    	VatNumber:favVat,
	}
	
	mf.ID = mf.GetID()
	mc := &doc.MyFav{
		ID:mf.ID,
		CompanyID:comID, 
	}
	
	err := sm.dbmodel.FindByID(mc)
	if err == nil {
        err = sm.dbmodel.Update(mf, ui)
	} else if err.Error() != "invalid id" {
        err = sm.dbmodel.Save(mf, ui)
    } else {
		return err
	}
	
	if err != nil{
		return err
	}
	return nil	
}
